<?php
$host = "localhost";
$user = "root";
$password = "";
$db = "seconddb";

session_start();

$data = mysqli_connect($host, $user, $password, $db);

if ($data === false) {
    die("Connection error");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {~
    $username = $_POST["username"];
    $password = $_POST["password"];

    $sql = "SELECT * FROM login WHERE username='$username'";

    $result = mysqli_query($data, $sql);
    $row = mysqli_fetch_array($result);
    if ($row && password_verify($password , $row['password'] )) {
        $_SESSION["username"] = $username;

        if ($row["role_id"] == "1") {
            header("location: admin.php");
        } else {
            header("location: user.php");
        }
    } else {
        echo "Username or password incorrect";
    }
}

mysqli_close($data);
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://classless.de/classless.css">
    <title>Login Form</title>
</head>
<body>
    <h1>Login Form</h1>
    <form action="#" method="POST">
        <label>Username</label>
        <input type="text" name="username" required>

        <label>Password</label>
        <input type="password" name="password" required>

        <input type="submit" value="Login">
    </form>
    <a href="register.php">Don't have an account yet!!!</a>
</body>
</html>
