<?php
$host = "localhost";
$user = "root";
$password = "";
$db = "seconddb";

session_start();

// Create a MySQLi connection
$conn = new mysqli($host, $user, $password, $db);

// Check if the connection was successful
if ($conn->connect_error) {
    die("Connection error: " . $conn->connect_error);
}

// Function to check if the old password matches the one stored in the database
function checkOldPassword($conn, $username, $oldPassword)
{
    // Prepare the SQL statement
    $query = "SELECT password FROM login WHERE username = ?";

    // Prepare the statement
    $statement = $conn->prepare($query);

    if ($statement === false) {
        die("Error in preparing statement: " . $conn->error);
    }

    // Bind the username parameter
    $statement->bind_param("s", $username);

    // Execute the statement
    $statement->execute();

    // Bind the result
    $statement->bind_result($hashedPassword);

    // Fetch the result
    $statement->fetch();

    // Close the statement
    $statement->close();

    // Check if the old password matches the stored hashed password
    if (password_verify($oldPassword, $hashedPassword)) {
        return true; // Passwords match
    } else {
        return false; // Passwords do not match
    }
}

// Function to update the password in the database
function updatePassword($conn, $username, $newPassword)
{
    // Hash the new password
    $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

    // Prepare the SQL statement
    $query = "UPDATE login SET password = ? WHERE username = ?";

    // Prepare the statement
    $statement = $conn->prepare($query);

    if ($statement === false) {
        die("Error in preparing statement: " . $conn->error);
    }

    // Bind the parameters
    $statement->bind_param("ss", $hashedPassword, $username);

    // Execute the statement
    $result = $statement->execute();

    // Close the statement
    $statement->close();

    return $result;
}

// Check if the form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the form data
    $oldPassword = $_POST['old_password'];
    $newPassword = $_POST['new_password'];
    $username = $_SESSION['username']; // Assuming the username is stored in the session

    // Validate the old password
    if (!checkOldPassword($conn, $username, $oldPassword)) {
        echo "Old password is incorrect.";
        exit;
    }
    
    // Update the password
    if (updatePassword($conn, $username, $newPassword)) {
        echo "Password updated successfully!";
    } else {
        echo "Failed to update the password.";
    }
}

// Close the database connection
$conn->close();
?>

<!-- HTML form to collect the old and new passwords -->
<html>
    <head>
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css"> -->
<script src="editPassword.js"></script>
<script src="https://cdn.tailwindcss.com"></script>
</head>

    <body class="">
        <div class="border-2 border-red-500 text-center m-auto mt-40 w-60 h-60 bg-gray-300">
            <form method="post" action="" onsubmit="return passwordValidate()"; class="">
                <div class="p-2 ">
                <label for="old_password" class="font-bold">Old Password</label>
                <input type="password" name="old_password" id="old_password" required class="border-2 border-black">
                </div>
                <div  class="p-2 ">
                <label for="new_password" class="font-bold ">New Password</label>
                <input type="password" name="new_password" id="new_password" required class="border-2 border-black">
                </div>
                <div  class="p-2 ">
                <label for="confirm" class="font-bold ">Confirm Password</label>
                <input type="confirm" name="confirm" id="confirm" required class="border-2 border-black">
                </div>
                <div class="pb-2">
                <input type="submit" value="Update Password"name="submit" class="hover:bg-blue-700 border-2 bg-transparent cursor-pointer ">
                </div>
            </form>
        </div>
    </body>
</html>