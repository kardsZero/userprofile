<?php

session_start();

if(!isset($_SESSION["username"]))
{
	header("location:login.php");
}

$host = "localhost";
$user = "root";
$password = "";
$db = "seconddb";



$data = mysqli_connect($host, $user, $password, $db);

if ($data === false) {
    die("Connection error");
}

// Retrieve the user's profile from the database
$username = $_SESSION['username']; // Assuming you have stored the user ID in a session
$query = "SELECT * FROM login WHERE username = '$username'";
$result = mysqli_query($data, $query);

if (mysqli_num_rows($result) > 0) {
  $row = mysqli_fetch_assoc($result);
  $username = $row['username'];
  $password = $row['password'];
  $email = $row['email'];
  $phone = $row['phone'];

} else {
  echo "User profile not found.";
}

// Handle the form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Retrieve the updated profile data
  $updatedUsername = $_POST['username'];
  $updatedEmail = $_POST['email'];
  $updatedPhone = $_POST['phone'];
  // ... Retrieve other updated profile fields as needed

  // Update the user's profile in the database
  $updateQuery = "UPDATE login SET username = '$updatedUsername', email = '$updatedEmail', phone = '$updatedPhone' WHERE username = '$username'";
  $updateResult = mysqli_query($data, $updateQuery);

  if ($updateResult) {
    echo "Profile updated successfully.";
    // You can also redirect the user to another page after successful update
    header("Location: edit.php");
    exit;
  } else {
    echo "Profile update failed. Please try again.";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Edit Profile</title>
</head>
<body>
  <h2>Edit Profile</h2>
  <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="username">Username:</label>
    <input type="text" name="username" value="<?php echo $username; ?>"><br>



    <label for="email">Email:</label>
    <input type="email" name="email" value="<?php echo  $email; ?>"><br>

    <label for="phone">Phone No:</label>
    <input type="text" name="phone" value="<?php echo $phone; ?>"><br>

    <!-- Add other profile fields here as needed -->

    <input type="submit" value="Update Profile">
  </form>
    <a href="editPassword.php">Edit your password here</a>
</body>
</html>
