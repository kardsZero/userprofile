<?php
$host = "localhost";
$user = "root";
$password = "";
$db = "seconddb";

session_start();

$data = mysqli_connect($host, $user, $password, $db);

if ($data === false) {
    die("Connection error");
}

if (isset($_GET['delete_id'])) {
    $deleteId = $_GET['delete_id'];
    $deleteQuery = "DELETE FROM login WHERE id = '$deleteId'";
    mysqli_query($data, $deleteQuery);
}

$query = "SELECT l.id, l.username, l.email, l.phone, l.country, r.role FROM roletb r INNER JOIN login l ON l.role_id = r.id WHERE l.role_id <> 1";
$result = mysqli_query($data, $query);

if (!$result) {
    die("Query error: " . mysqli_error($data));
}

$rowcount = mysqli_num_rows($result);

?>

<html>
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Admin Page</title>
   <style>
      table {
         width: 100%;
         border-collapse: collapse;
      }

      table, th, td {
         border: 1px solid black;
         padding: 8px;
      }

      th {
         background-color: gray;
      }

      p {
         text-align: center;
         font-size: 25px;
         font-weight: 900;
      }

      .user-table {
         
      }
   </style>
</head>
<body>
   <div class="container">
      <div class="content">
         <table>
            <tr class="user-table">
               <td colspan="6"><p>User's table</p></td>
            </tr>
            <tr>
               <th>Name</th>
               <th>Email Address</th>
               <th>Phone no.</th>
               <th>Country</th>
               <th>Role</th>
               <th>Action</th>
            </tr>
            <?php
            while ($row = mysqli_fetch_array($result)) {
               ?>
               <tr>
                  <td><?php echo $row['username']; ?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['phone']; ?></td>
                  <td><?php echo $row['country']; ?></td>
                  <td><?php echo $row['role']; ?></td>
                  <td>
                     <a href="display.php?delete_id=<?= $row['id']; ?>" onclick="return confirm('Are you sure you want to delete this user?')">Delete</a>
                  </td>
               </tr>
               <?php
            }
            ?>
           
         </table>
      </div>
   </div>
   <a href="logout.php">Logout</a><br><a href="admin.php">Back to HomePage</a>
</body>
</html>
